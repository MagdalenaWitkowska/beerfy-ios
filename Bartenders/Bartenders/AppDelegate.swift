//
//  AppDelegate.swift
//  Bartenders
//
//  Created by Magdalena Witkowska on 06.02.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    
        FBLoginView.self
        FBProfilePictureView.self


        self.window?.rootViewController = UIStoryboard(name: "FirstView", bundle: nil).instantiateInitialViewController() as? UIViewController
        setupAppearance()
        if UserDefaults.isUserLoggedIn().0 == true {
            ApiController.refresh(UserDefaults.isUserLoggedIn().1, handler: { (success, error) -> Void in
                if success == true {
                    var qrcodeVC = QRCodeViewController()
                    var user : User!
                    if let email: String = NSUserDefaults.standardUserDefaults().valueForKey("email") as? String {
                        if let accessToken = NSUserDefaults.standardUserDefaults().valueForKey("access_token") as? String {
                            if let username = NSUserDefaults.standardUserDefaults().valueForKey("username") as? String {
                                
                                user = User(username: username, accessToken: accessToken, email: email)
                                qrcodeVC.user = user
                            }
                        }
                    }
                    var navController = UINavigationController(rootViewController: qrcodeVC)
                    self.window?.rootViewController? = navController
                    UIView.transitionWithView(self.window!, duration: 0.3, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: { () -> Void in
                        self.window!.rootViewController = navController
                    }, completion: nil)
                    

                } else {
                    self.showLoginView()
                }
            })
        } else {
            var login = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController() as! LoginViewController
            UIView.transitionWithView(self.window!, duration: 2.3, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: { () -> Void in
                self.window!.rootViewController = login
                }, completion: nil)
        }
        
        
        // Override point for customization after application launch.
        return true
    }
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        var wasHandled:Bool = FBAppCall.handleOpenURL(url, sourceApplication: sourceApplication)
        return wasHandled
    }
 

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func setupAppearance(){
        UITextField.appearance().tintColor = BartendersColor.bartendersYellow()
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        
        UINavigationBar.appearance().backgroundColor = BartendersColor.bartendersYellow()
        UINavigationBar.appearance().barTintColor = BartendersColor.bartendersYellow()
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().alpha = 1.0
        UINavigationBar.appearance().setBackgroundImage(UIImage(), forBarPosition: .Any, barMetrics: .Default)
        UINavigationBar.appearance().shadowImage = UIImage()
    }
    
    func showLoginView(){
        var login = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController() as! LoginViewController
        UIView.transitionWithView(self.window!, duration: 2.3, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: { () -> Void in
            self.window!.rootViewController = login
            }, completion: nil)
    }


}

