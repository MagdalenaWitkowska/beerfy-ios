//
//  ViewController.swift
//  Bartenders
//
//  Created by Magdalena Witkowska on 06.02.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, FBLoginViewDelegate {

    @IBOutlet weak var loginAndEnjoyLabel : UILabel!
    @IBOutlet weak var loginTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!
    @IBOutlet weak var loginButton : UIButton!
    @IBOutlet weak var dontHaveAnAccountButton : UIButton!
    @IBOutlet weak var loginAndPasswordView : UIView!
    @IBOutlet weak var logoImageView : UIImageView!
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet var fbLoginView : FBLoginView!
    var alertLabel : UILabel!
    var textFieldsUp : Bool! = false
    var heightToMove : CGFloat!
    var errorLabel : UILabel!
    var errorIsShown : Bool! = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupAppearance()
        
        self.fbLoginView.delegate = self
        self.fbLoginView.readPermissions = ["public_profile", "email", "user_friends"]
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.passwordTextField.text = ""
        self.loginTextField.text = ""
        self.hideErrorAlert()
    }
    
    
    func setupAppearance(){
        self.loginAndEnjoyLabel.font = UIFont(name: "Comfortaa", size: 17.0)
        self.loginTextField.font = UIFont(name: "Comfortaa", size: 17.0)
        self.passwordTextField.font = UIFont(name: "Comfortaa", size: 17.0)
        self.loginTextField.layer.cornerRadius = 4.0
        self.passwordTextField.layer.cornerRadius = 4.0
        var tap = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        var swipe = UISwipeGestureRecognizer(target: self, action: "dismissKeyboard")
        swipe.direction = UISwipeGestureRecognizerDirection.Down
        self.view.addGestureRecognizer(tap)
        self.view.addGestureRecognizer(swipe)
        self.loginTextField.leftViewMode = UITextFieldViewMode.Always
        self.passwordTextField.leftViewMode = UITextFieldViewMode.Always
        self.loginTextField.leftView?.frame = CGRectMake(0,0, 33, 13)
        self.passwordTextField.leftView?.frame = CGRectMake(0, 0, 33, 13)
        var lock = UIImageView(frame: CGRectMake(0, 0, 33, 13))
        var at = UIImageView(frame: CGRectMake(0, 0, 33, 13))
        lock.image = UIImage(named: "lock")
        at.image = UIImage(named: "at")
        self.loginTextField.leftView = at
        self.loginTextField.leftView?.contentMode = UIViewContentMode.ScaleAspectFit
        self.passwordTextField.leftView?.contentMode = UIViewContentMode.ScaleAspectFit
        self.passwordTextField.leftView = lock
        var attributes = [NSForegroundColorAttributeName : UIColor.lightGrayColor()]
        var attributedLogin = NSAttributedString(string: NSLocalizedString("Your email", comment:""), attributes: attributes)
        self.loginTextField.attributedPlaceholder = attributedLogin
        var attributedPassword = NSAttributedString(string: NSLocalizedString("Your password", comment:""), attributes: attributes)
        self.passwordTextField.attributedPlaceholder = attributedPassword
        self.passwordTextField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
        self.passwordTextField.addTarget(self, action: "textFieldEditingDidBegin", forControlEvents: UIControlEvents.EditingDidBegin)
        self.loginTextField.addTarget(self, action: "textFieldEditingDidBegin", forControlEvents: UIControlEvents.EditingDidBegin)
        self.passwordTextField.addTarget(self, action: "moveTextFieldsUp:", forControlEvents: UIControlEvents.EditingDidBegin)
        self.loginTextField.addTarget(self, action: "moveTextFieldsUp:", forControlEvents: UIControlEvents.EditingDidBegin)
        self.loginButton.titleLabel?.font = UIFont(name: "Comfortaa", size: 18.0)
        self.loginButton.layer.cornerRadius = 4.0
        self.loginButton.layer.masksToBounds = false
        self.loginButton.layer.shadowColor = BartendersColor.bartendersDarkOrange().CGColor
        self.loginButton.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.loginButton.layer.shadowRadius = 0.0
        self.loginButton.layer.shadowOpacity = 1.0
        self.dontHaveAnAccountButton.titleLabel?.font = UIFont(name: "Comfortaa", size: 14.0)
        self.errorLabel = UILabel(frame: CGRectMake(self.passwordTextField.frame.origin.x, self.passwordTextField.frame.origin.y + self.passwordTextField.frame.size.height/2, self.passwordTextField.frame.size.width
            , self.passwordTextField.frame.size.height/2))
        self.errorLabel.backgroundColor = UIColor(red: 241.0/255.0, green: 15.0/255.0, blue: 60.0/255.0, alpha: 1.0)
        self.errorLabel.layer.cornerRadius = 4.0
        self.errorLabel.layer.masksToBounds = true
        self.errorLabel.textColor = UIColor.whiteColor()
        self.errorLabel.textAlignment = .Center
        self.errorLabel.font = UIFont(name: "Comfortaa", size: 17)
        self.errorLabel.textAlignment = NSTextAlignment.Center
        self.errorLabel.hidden = true
        self.loginAndPasswordView.addSubview(self.errorLabel)
        self.loginAndPasswordView.bringSubviewToFront(self.passwordTextField)
        
    }
    
    func dismissKeyboard(){
        if self.textFieldsUp == true {
            self.view.endEditing(true)
            self.textFieldsUp = false
            UIView.animateWithDuration(0.4, animations: { () -> Void in
                var logoFrame = self.logoImageView.frame
                logoFrame.origin.y =  logoFrame.origin.y + self.heightToMove
                self.logoImageView.frame = logoFrame
                var frameLoginAndPass = self.loginAndPasswordView.frame
                frameLoginAndPass.origin.y = frameLoginAndPass.origin.y + self.heightToMove - 40
                self.loginAndPasswordView.frame = frameLoginAndPass
            })
        }
    }
    
    func textFieldDidChange(sender : AnyObject) {
        var textField = sender as! UITextField
        if textField == self.passwordTextField {
            if textField.text == "" {
                textField.font = UIFont(name: "Comfortaa", size: 17.0)
            } else {
                textField.font = UIFont.systemFontOfSize(17.0)
            }
            
        }
    
    }
    
    func textFieldEditingDidBegin(){
        if self.errorIsShown == true {
            self.hideErrorAlert()
        }
    }
    
    
    
    func moveTextFieldsUp(sender : AnyObject){
        if self.textFieldsUp == false {
            self.textFieldsUp = true
            UIView.animateWithDuration(0.4, animations: { () -> Void in
                var logoFrame = self.logoImageView.frame
                self.heightToMove = logoFrame.size.height + logoFrame.origin.y
                logoFrame.origin.y =  -logoFrame.size.height
                self.logoImageView.frame = logoFrame
                var frameLoginAndPass = self.loginAndPasswordView.frame
                frameLoginAndPass.origin.y = frameLoginAndPass.origin.y - self.heightToMove + 40
                self.loginAndPasswordView.frame = frameLoginAndPass
            })

        }
    }

    //MARK: @IBActions
    
    @IBAction func loginClicked(){
        self.dismissKeyboard()
        if count(self.loginTextField.text) == 0 || count(self.passwordTextField.text) == 0 {
            self.showErrorAlert(NSLocalizedString("Provide credentials.", comment: ""))
            return
        }
        CustomIndicatorView.show()
        ApiController.login(self.loginTextField.text, password: self.passwordTextField.text) { (success, error, user) -> Void in
            CustomIndicatorView.hide()
            if error != nil {
                self.showErrorAlert(error!)
            } else {
                if success == true {
                    var qrcodeVC = QRCodeViewController()
                    qrcodeVC.user = user
                    var navController = UINavigationController(rootViewController: qrcodeVC)
                    self.presentViewController(navController, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    @IBAction func dontHaveAccountClicked(){
        UIApplication.sharedApplication().openURL(NSURL(string: "http://beerfy.com")!)
    }
    
    func showErrorAlert(errorText : String){
        if self.errorIsShown == false {
            self.errorLabel.text = errorText
            self.errorIsShown = true
            self.errorLabel.hidden = false
            UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                self.errorLabel.numberOfLines = 0
                self.errorLabel.sizeToFit()
                var heightOfErrLabel = self.errorLabel.frame.size.height
                var frameOfLoginButton = self.loginButton.frame
                frameOfLoginButton.origin.y = frameOfLoginButton.origin.y + heightOfErrLabel
                self.loginButton.frame = frameOfLoginButton
                var frameOfFacebookButton = self.fbLoginView.frame
                frameOfFacebookButton.origin.y = frameOfFacebookButton.origin.y + heightOfErrLabel
                self.fbLoginView.frame = frameOfFacebookButton
                self.errorLabel.frame = CGRectMake(self.errorLabel.frame.origin.x, CGRectGetMaxY(self.passwordTextField.frame), self.loginButton.frame.size.width, heightOfErrLabel)
                }) { (Bool) -> Void in
                }
        }

    }
        
    func hideErrorAlert(){
        if self.errorIsShown == true {
            UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                var frameOfLoginButton = self.loginButton.frame
                frameOfLoginButton.origin.y = frameOfLoginButton.origin.y - self.errorLabel.frame.size.height
                self.loginButton.frame = frameOfLoginButton
                var frameOfFacebookButton = self.fbLoginView.frame
                frameOfFacebookButton.origin.y = frameOfFacebookButton.origin.y - self.errorLabel.frame.size.height
                self.fbLoginView.frame = frameOfFacebookButton
                self.errorLabel.frame = CGRectMake(self.errorLabel.frame.origin.x, CGRectGetMinY(self.passwordTextField.frame), self.errorLabel.frame.size.width, self.errorLabel.frame.size.height)
                }) { (Bool) -> Void in
                    self.errorLabel.hidden = true
                    self.errorIsShown = false
                }
        }
        
    }
    
    //MARK: Facebook Delegate Methods
   
    func loginViewShowingLoggedInUser(loginView : FBLoginView!) {
        println("User Logged In")
        println("This is where you perform a segue.")
        dismissKeyboard()
        CustomIndicatorView.show()
        var fbAccessToken = FBSession.activeSession().accessTokenData.accessToken
        var expirationDate = FBSession.activeSession().accessTokenData.expirationDate
        println(expirationDate)
        println(NSDate())
        var sec = round(expirationDate.timeIntervalSinceDate(NSDate()))
        var secStr = NSString(format: "%i", sec)
        ApiController.loginWithFacebookToken(fbAccessToken, expirationDate: secStr as String, handler: { (success, error, user) -> Void in
            
            if error != nil {
                CustomIndicatorView.hide()
               // FBSession.activeSession().closeAndClearTokenInformation()
                self.showErrorAlert(error!)
            } else {
                if success == true {
                    var qrcodeVC = QRCodeViewController()
                    qrcodeVC.user = user
                    var navController = UINavigationController(rootViewController: qrcodeVC)
                    CustomIndicatorView.hide()
                    self.presentViewController(navController, animated: true, completion: nil)
                }
            }
        })
        
    }

    func loginViewFetchedUserInfo(loginView : FBLoginView!, user: FBGraphUser){
        
        println("User Name: \(user.name)")
        println("ObjectID: \(user.objectID)")
    }
    
    func loginViewShowingLoggedOutUser(loginView : FBLoginView!) {
        println("User Logged Out")
    }
    
    func loginView(loginView : FBLoginView!, handleError:NSError) {
        println("Error: \(handleError.localizedDescription)")
    }



}

