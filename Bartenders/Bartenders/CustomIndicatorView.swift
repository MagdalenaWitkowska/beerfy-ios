//
//  CustomIndicatorView.swift
//  Bartenders
//
//  Created by Magdalena Witkowska on 20.02.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit

class CustomIndicatorView: UIView {

    var counter = 0
    var activityIndicator : UIActivityIndicatorView!
    
    class var sharedInstance : CustomIndicatorView {
        
        struct Static {
            static let instance : CustomIndicatorView = CustomIndicatorView()
        }
        return Static.instance
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    init() {
        super.init(frame: CGRectMake(0,0,0,0))
        var mainWindow = UIApplication.sharedApplication().keyWindow
        if let main = mainWindow {
            self.frame = CGRectMake((mainWindow!.frame.width - 70)/2, (mainWindow!.frame.height-70)/2, 70, 70)
        }
        self.layer.cornerRadius = 20
        self.backgroundColor = BartendersColor.bartendersLightOrange()
        self.alpha = 0.0
        self.activityIndicator = UIActivityIndicatorView(frame: CGRectMake(15, 15, 40, 40))
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        self.addSubview(self.activityIndicator)
        self.bringSubviewToFront(self.activityIndicator)
        
    }
    
    private func _show(){
        var mainWindow = UIApplication.sharedApplication().keyWindow
        self.alpha = 0
        self.counter++
        if self.counter == 1 {
            mainWindow?.addSubview(self)
            mainWindow?.bringSubviewToFront(self)
            mainWindow?.userInteractionEnabled = false
            UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
                self.alpha = 0.9
                }, completion: { (Bool) -> Void in
                    self.activityIndicator.startAnimating()
                    
            })
        }
    }
    
    private func _hide(){
        var mainWindow = UIApplication.sharedApplication().keyWindow
        self.counter--
        self.counter < 0 ? 0 : self.counter
        if self.counter == 0 {
            UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
                self.alpha = 0.9
            }, completion: { (Bool) -> Void in
                self.activityIndicator.stopAnimating()
                self.removeFromSuperview()
                mainWindow?.userInteractionEnabled = true
            })
        }
    }
    
    class func hide(){
        CustomIndicatorView.sharedInstance._hide()
    }
    
    class func show(){
        CustomIndicatorView.sharedInstance._show()
    }
    

    

}
