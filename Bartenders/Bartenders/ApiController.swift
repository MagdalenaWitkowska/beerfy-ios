//
//  ApiController.swift
//  Bartenders
//
//  Created by Magdalena Witkowska on 20.02.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit



#if BEERFY_BETA
    //dom
    let path = "http://192.168.0.12:6543/api/v1/"
    //praca
    //let path = "http://10.9.200.162:6543/api/v1/"
#else
    let path = "https://beerfy.com/api/v1/"
#endif

//let path = "https://beerfy.com/api/v1/"
//let path = "http://192.168.0.12:6543/api/v1/"
//let path = "http://10.9.200.162:6543/api/v1/"
class ApiController: NSObject {
    class var sharedInstance : ApiController {
        
        struct Static {
            static let instance : ApiController = ApiController()
        }
        return Static.instance
        
    }
    
    var accessToken : String!
    
    class func setURLRequestWithEndpoint(endpoint: String, method: String, authorization : String?, parameters : [String:String]?) -> NSURLRequest{
        
        let URL = NSURL(string: path.stringByAppendingString(endpoint))
        
        let mutableURLRequest = NSMutableURLRequest(URL: URL!)
        mutableURLRequest.HTTPMethod = method
        mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        if let auth = authorization {
            mutableURLRequest.setValue(authorization!, forHTTPHeaderField: "Authorization")
        }
        if let params = parameters {
            var JSONSerializationError: NSError? = nil
            mutableURLRequest.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &JSONSerializationError)
            
        }
        return mutableURLRequest
        
    }
    
    class func handleError(error : NSError) {
        var noConnectionError = [-1004, -1005, -1009]
        if contains(noConnectionError, error.code) {
            ConnectionErrorAlert(message: NSLocalizedString("You are offline.", comment:"")).show()
        }
        else if error.code == -1001 {
            ConnectionErrorAlert(message: NSLocalizedString("Connection has timed out.", comment:"")).show()

        }
        else {
            ConnectionErrorAlert(message: NSLocalizedString("Something went wrong.", comment:"")).show()
        }
    }
    
    // MARK: - Login with login and password
    class func login(login : NSString, password : NSString, handler: ((success: Bool, error: String?, user: User?) -> Void)) {

        let parameters:[String:AnyObject]? = [
            "login"         : login,
            "password"      : password,
        ];
        var plainString = (login as String)+":"+(password as String)
        let plainData = plainString.dataUsingEncoding(NSUTF8StringEncoding)
        var base64String = plainData?.base64EncodedStringWithOptions(.allZeros)
        base64String =  "Basic " + base64String!
        var mutableRequest = setURLRequestWithEndpoint("auth/barman", method: "POST", authorization: base64String!, parameters: nil)
        
        request(mutableRequest)
        .responseJSON(options: NSJSONReadingOptions.AllowFragments) { (request, response, JSON, error) -> Void in
            println(request)
            println(response)
            println("JSON \(JSON)")
            println(error)
            
            if error != nil {
                self.handleError(error!)
                handler(success: false, error: nil, user:nil)
            } else {
                let jsonDictionary = JSON as! NSDictionary
                if let errors = jsonDictionary["errors"] as? NSArray {
                    if let descriptionError = errors[0]["description"] as? String {
                        handler(success: false, error: descriptionError, user: nil)
                    }
                    
                } else {
                    if let messages = jsonDictionary["messages"] as? NSArray {
                        if let message = messages[0] as? String {
                            self.sharedInstance.accessToken = base64String
                            
                            
                            if let user = Mapper<User>().map(JSON) {
                                if let accessToken = user.accessToken {
                                    UserDefaults.saveToUserDefaults("access_token", value: accessToken)
                                    if let email = user.email {
                                        UserDefaults.saveToUserDefaults("email", value: email)
                                    }
                                    if let username = user.username {
                                        UserDefaults.saveToUserDefaults("username", value: username)
                                    }
                                }
                                
                                handler(success: true, error: nil, user: user)
                                
                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - GET Login with access_token from Facebook
    class func loginWithFacebookToken(facebookToken : String, expirationDate: String, handler: ((success: Bool, error: String?, user: User?) -> Void)) {
      
        var plainString = "Facebook"+" "+facebookToken+" "+expirationDate
        let plainData = plainString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData?.base64EncodedStringWithOptions(.allZeros)
        
        var mutableRequest = setURLRequestWithEndpoint("auth/barman", method: "POST", authorization: plainString, parameters: nil)
        println("Facebook "+facebookToken+" "+expirationDate)
        request(mutableRequest)
            .responseJSON { (request, response, JSON, error) -> Void in
                println(request)
                println(response)
                println("JSON \(JSON)")
                println(error)
                
                if error != nil {
                    self.handleError(error!)
                    handler(success: false, error: nil, user:nil)
                } else {
                    let jsonDictionary = JSON as! NSDictionary
                    if let errors = jsonDictionary["errors"] as? NSArray {
                        if let descriptionError = errors[0]["description"] as? String {
                            handler(success: false, error: descriptionError, user: nil)
                        }
                        
                    } else {
                        if let messages = jsonDictionary["messages"] as? NSArray {
                            if let message = messages[0] as? String {
                                //self.sharedInstance.accessToken = base64String
                                
                                
                                if let user = Mapper<User>().map(JSON) {
                                    if let accessToken = user.accessToken {
                                        UserDefaults.saveToUserDefaults("access_token", value: accessToken)
                                        if let email = user.email {
                                            UserDefaults.saveToUserDefaults("email", value: email)
                                        }
                                        if let username = user.username {
                                            UserDefaults.saveToUserDefaults("username", value: username)
                                        }
                                    }
                                    
                                    handler(success: true, error: nil, user: user)
                                    
                                }
                            }
                        }
                    }
                }
        }
    }
    
    // MARK: - GET Refresh access token
    class func refresh(token : String, handler: ((success: Bool, error: String?) -> Void)) {

        var parameter = ["access_token":token]
        var mutableRequest = setURLRequestWithEndpoint("auth/refresh?access_token="+token, method: "GET", authorization: token, parameters: nil)
        request(mutableRequest)
            .responseJSON(options: NSJSONReadingOptions.AllowFragments)  { (request, response, JSON, error) -> Void in
                println(request)
                println(response)
                //println("JSON \(JSON)")
                //println(error)
                if response?.statusCode == 200 {
                    handler(success: true, error: nil)
                } else {
                    handler(success: false, error: nil)
                }
        }
    }
    
    class func validateQRCode(QRcode : String, handler: ((success: Bool, error: String?, campaign: Campaign?) -> Void)){
        var mutableRequest = setURLRequestWithEndpoint("coupons/"+QRcode, method: "PUT", authorization: self.sharedInstance.accessToken, parameters: nil)
        
        request(mutableRequest)
        .responseJSON(options: NSJSONReadingOptions.AllowFragments) { (request, response, JSON, error) -> Void in
            println(request)
            println(response)
            println("JSON \(JSON)")
            println(error)
            
            if error != nil {
                self.handleError(error!)
                if error?.code == 3840 {
                    handler(success: false, error: "Invalid", campaign: nil)
                } else {
                    handler(success: false, error: nil, campaign: nil)
                }
                
            } else {
                let jsonDictionary = JSON as! NSDictionary
                if let errors = jsonDictionary["errors"] as? NSArray {
                    if let descriptionError = errors[0]["description"] as? String {
                        handler(success: false, error: descriptionError, campaign: nil)
                    }  
                } else {
                    if let campaign = Mapper<Campaign>().map(JSON) {
                        handler(success: true, error: nil, campaign: campaign)
                    }
                }
            }
        }
        
    }
    
    
}
