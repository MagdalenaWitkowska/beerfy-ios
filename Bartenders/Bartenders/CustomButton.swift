//
//  CustomButton.swift
//  Bartenders
//
//  Created by Magdalena Witkowska on 19.02.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        let pointSize = self.titleLabel?.font.pointSize as CGFloat?
        self.titleLabel?.font = UIFont(name: "Comfortaa", size: 17.0)
    }

}
