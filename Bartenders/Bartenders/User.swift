//
//  User.swift
//  Bartenders
//
//  Created by Magdalena Witkowska on 22.02.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit

class User: Mappable {
    
    var username: String?
    var accessToken: String?
    var email: String?

    
    
    required init?(_ map: Map) {
        mapping(map)
    }
    
    required init(username: String, accessToken : String, email : String){
        self.username = username
        self.accessToken = accessToken
        self.email = email
        
    }
    
    // Mappable
    func mapping(map: Map) {
        username <- map["user.username"]
        email <- map["user.email"]
        accessToken <- map["access_token"]

    }
}

