//
//  UserDefaults.swift
//  Beerfy
//
//  Created by Magdalena Witkowska on 15.03.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import Foundation
import UIKit


class UserDefaults {
    
    class func saveToUserDefaults(key : String, value : AnyObject){
        NSUserDefaults.standardUserDefaults().setValue(value, forKey: key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    class func clearUserDefaults(key : String){
        NSUserDefaults.standardUserDefaults().setValue("", forKey: key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    class func isUserLoggedIn() -> (Bool, String){
        if let token: String = NSUserDefaults.standardUserDefaults().valueForKey("access_token") as? String{
            if token != "" {
                return (true, token)
            }
            else {
                return (false, "")
            }
        }
        return (false, "")
    }

    
}