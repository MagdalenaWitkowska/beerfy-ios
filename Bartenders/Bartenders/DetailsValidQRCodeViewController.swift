//
//  DetailsValidQRCodeViewController.swift
//  Bartenders
//
//  Created by Magdalena Witkowska on 25.02.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit

class DetailsValidQRCodeViewController: UIViewController {

    @IBOutlet var okButton : UIButton!
    @IBOutlet var campaignNameLabel : UILabel!
    @IBOutlet var campaignDescription : UILabel!
    @IBOutlet var campaignLocation : UILabel!
    @IBOutlet var endOfCampaignLabel : UILabel!
    @IBOutlet var endDateLabel : UILabel!
    var campaign : Campaign!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.okButton.titleLabel?.font = UIFont(name: "Comfortaa", size: 18.0)
        self.okButton.layer.cornerRadius = 4.0
        self.okButton.layer.masksToBounds = false
        self.okButton.layer.shadowColor = BartendersColor.bartendersDarkOrange().CGColor
        self.okButton.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.okButton.layer.shadowRadius = 0.0
        self.okButton.layer.shadowOpacity = 1.0
        
        if let name = self.campaign.name {
            self.campaignNameLabel.text = NSString(string: self.campaign.name!).uppercaseString
        }
        var currentLocale: String = NSBundle.mainBundle().preferredLocalizations.first as! String
        if currentLocale == "pl" {
            if let desc = self.campaign.descPL {
                self.campaignDescription.text = desc
            }
        } else {
            if let desc = self.campaign.descEN {
                self.campaignDescription.text = desc
            }
        }
        
        if let pubname = self.campaign.pubname {
            if let street = self.campaign.street {
                self.campaignLocation.text = self.campaign.pubname! + ", " + self.campaign.street!
            } else {
                self.campaignLocation.text = self.campaign.pubname!
            }
        } else {
            if let street = self.campaign.street {
                self.campaignLocation.text = self.campaign.street!
            }
        }
        if self.campaign.showEndDate == 0 {
            self.endOfCampaignLabel.hidden = true
            self.endDateLabel.hidden = true
        } else {
            self.endOfCampaignLabel.hidden = false
            self.endDateLabel.hidden = false
            if let endsAt = campaign.endsAt {
                var dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd-MM-YYY HH:mm"
                self.endDateLabel.text = dateFormatter.stringFromDate(self.campaign.endsAt!)
            }
        }
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func dismiss(){
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }

}
