//
//  CustomLabel.swift
//  Bartenders
//
//  Created by Magdalena Witkowska on 19.02.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit

class CustomLabel: UILabel {

    override func layoutSubviews() {
        super.layoutSubviews()
        self.font = UIFont(name: "Comfortaa", size: self.font.pointSize)
    }
}
