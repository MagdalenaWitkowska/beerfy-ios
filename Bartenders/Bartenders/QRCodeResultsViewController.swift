//
//  QRCodeResultsViewController.swift
//  Bartenders
//
//  Created by Magdalena Witkowska on 24.02.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit

class QRCodeResultsViewController : UIViewController {
   
    @IBOutlet var detailButton : UIButton!
    @IBOutlet var nameOfCampaign : UILabel!
    @IBOutlet var errorLabel : UILabel!
    var errorMessage : String!
    var campaign : Campaign!
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let errLabel = errorLabel {
             errorLabel.text = errorMessage.uppercaseString
        }
        
        if let detailButton = self.detailButton {
            self.detailButton.titleLabel?.font = UIFont(name: "Comfortaa", size: 18.0)
            self.detailButton.layer.cornerRadius = 4.0
            self.detailButton.layer.masksToBounds = false
            self.detailButton.layer.shadowColor = BartendersColor.bartendersDarkOrange().CGColor
            self.detailButton.layer.shadowOffset = CGSize(width: 0, height: 4)
            self.detailButton.layer.shadowRadius = 0.0
            self.detailButton.layer.shadowOpacity = 1.0
        }
        
        if let nameOfCampaign = self.nameOfCampaign {
            if let campaign = self.campaign {
                self.nameOfCampaign.text = campaign.name
            }
            
        }
    }
    
    @IBAction func dismiss(){
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    
    @IBAction func detailsButtonClicked(){
        var detailsQRCode = UIStoryboard(name: "DetailsValidQRCode", bundle: nil).instantiateInitialViewController() as! DetailsValidQRCodeViewController
        detailsQRCode.campaign = self.campaign
        self.presentViewController(detailsQRCode, animated: true) { () -> Void in
            
        }
    }
}
