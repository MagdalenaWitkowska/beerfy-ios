//
//  ConnectionErrorAlert.swift
//  Bartenders
//
//  Created by Magdalena Witkowska on 20.02.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit

class ConnectionErrorAlert: UIView {

    var isShown : Bool = false
    
    init(message : String){
        super.init(frame: CGRectMake(0,0,0,0))
        var mainWindow = UIApplication.sharedApplication().keyWindow
        self.frame = CGRectMake(0, -60, mainWindow!.frame.width, 60)
        self.backgroundColor = BartendersColor.bartendersLightOrange()
        var label = UILabel(frame: CGRectMake(0, 20, self.frame.size.width, self.frame.size.height-20))
        label.text = message
        label.font = UIFont(name: "Comfortaa", size: 16.0)
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Center
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.addSubview(label)
        mainWindow?.addSubview(self)
        mainWindow?.bringSubviewToFront(self)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func show(){
        if self.isShown == false {
        UIView.animateWithDuration(0.4, delay: 0.3, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            var frameOfAlert = self.frame
            frameOfAlert.origin.y = 0
            self.frame = frameOfAlert
            }) { (Bool) -> Void in
                self.isShown = true
                self.hide()
            }
        }
        
    }
    
    
    func hide(){
        if self.isShown == true {
            UIView.animateWithDuration(0.4, delay: 2.2, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
                var frameOfAlert = self.frame
                frameOfAlert.origin.y = -60
                self.frame = frameOfAlert
                }) { (Bool) -> Void in
                    self.isShown = false
            }
        }
    }

}
