//
//  QRCodeViewController.swift
//  Bartenders
//
//  Created by Magdalena Witkowska on 22.02.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit
import AVFoundation


class QRCodeViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    var user : User!
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let email = user.email {
            self.title = email
        }
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Comfortaa", size: 16.0)!, NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "door.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "logout")
        
       
        let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        var error:NSError?
        let input: AnyObject! = AVCaptureDeviceInput.deviceInputWithDevice(captureDevice, error: &error)
        if (error != nil) {
            println("\(error?.localizedDescription)")
            return
        }

        captureSession = AVCaptureSession()
        captureSession?.addInput(input as! AVCaptureInput)
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        
        self.view.layer.addSublayer(videoPreviewLayer)
        captureSession?.startRunning()
        
        var viewContr: AnyObject = UIStoryboard(name: "QRCodeStoryboard", bundle: nil).instantiateInitialViewController()as! UIViewController
        self.qrCodeFrameView = viewContr.view
       
        self.qrCodeFrameView?.frame = UIScreen.mainScreen().bounds
        self.qrCodeFrameView?.layoutIfNeeded()
        self.view.addSubview(self.qrCodeFrameView!)
        self.view.bringSubviewToFront(self.qrCodeFrameView!)
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        captureSession?.startRunning()
    }
    
    override func viewWillDisappear(animated: Bool) {
        captureSession?.stopRunning()
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        if metadataObjects == nil || metadataObjects.count == 0 {
            return
        }
        
        let metadataObj = metadataObjects[0]as!
        AVMetadataMachineReadableCodeObject
        if metadataObj.type == AVMetadataObjectTypeQRCode {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            println(barCodeObject.bounds)
            if !CGRectContainsRect(CGRectMake(40, 123, 260, 223),barCodeObject.bounds) {
                return
            }
            captureSession?.stopRunning()
            if metadataObj.stringValue != nil {
                println(metadataObj.stringValue)
                if self.firstValidationOfQRCode(metadataObj.stringValue) == true {
                    CustomIndicatorView.show()
                    ApiController.validateQRCode(metadataObj.stringValue, handler: { (success, error, campaign) -> Void in
                        CustomIndicatorView.hide()
                        if success == false {
                            if error != nil {
                                var errorQRCode = UIStoryboard(name: "QRCodeResults", bundle: nil).instantiateViewControllerWithIdentifier("Error") as! QRCodeResultsViewController
                                errorQRCode.errorMessage = error
                                self.presentViewController(errorQRCode, animated: true, completion: { () -> Void in
                                    
                                })
                                return

                            }
                        } else {
                            var okQRCode = UIStoryboard(name: "QRCodeResults", bundle: nil).instantiateViewControllerWithIdentifier("okQRCode") as! QRCodeResultsViewController
                            okQRCode.campaign = campaign
                            self.presentViewController(okQRCode, animated: true, completion: { () -> Void in
                                
                            })
                        }
                        
                        
                    })
                } else {
                    var invalid = UIStoryboard(name: "QRCodeResults", bundle: nil).instantiateViewControllerWithIdentifier("Invalid") as! QRCodeResultsViewController
                    self.presentViewController(invalid, animated: true, completion: { () -> Void in
                        
                    })
                    return
                }
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func logout(){
        UserDefaults.clearUserDefaults("access_token")
        UserDefaults.clearUserDefaults("email")
        UserDefaults.clearUserDefaults("username")
        var login = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController() as! LoginViewController
        UIApplication.sharedApplication().keyWindow?.rootViewController = login
        
        FBSession.activeSession().closeAndClearTokenInformation()
    }
    
    //validation before sending qrcode to backend. If string contains "/" or a white space - it fails the test.
    func firstValidationOfQRCode(qrcodeString : NSString) -> Bool {
        if qrcodeString.rangeOfString(" ").location != NSNotFound || qrcodeString.rangeOfString("/").location != NSNotFound {
            return false
        }
        return true
    }


}
