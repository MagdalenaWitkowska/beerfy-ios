//
//  Campaign.swift
//  Bartenders
//
//  Created by Magdalena Witkowska on 25.02.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit

class Campaign : Mappable {
    
    var id: String?
    var name: String?
    var pubname: String?
    var street: String?
    var showEndDate : Int?
    var endsAt : NSDate?
    var descPL : String?
    var descEN : String?

    
    required init?(_ map: Map) {
        mapping(map)
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["campaign.id"]
        name <- map["campaign.name"]
        pubname <- map["pub.name"]
        street <- map["pub.street"]
        showEndDate <- map["campaign.show_end_date"]
        endsAt <- (map["campaign.ends_at"], DateTransform())
        descEN <- map["campaign.description.en"]
        descPL <- map["campaign.description.pl"]

    }
}