//
//  BartendersColor.swift
//  Bartenders
//
//  Created by Magdalena Witkowska on 15.02.2015.
//  Copyright (c) 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit

class BartendersColor: UIColor {
   
    class func bartendersYellow() -> UIColor {
        return UIColor(red: 241.0/255.0, green: 196.0/255.0, blue: 15.0/255.0, alpha: 1.0)
    }
    class func bartendersLightOrange() -> UIColor {
        return UIColor(red: 238.0/255.0, green: 138.0/255.0, blue: 18.0/255.0, alpha: 1.0)
    }
    class func bartendersDarkOrange() -> UIColor {
        return UIColor(red: 206.0/255.0, green: 122.0/255.0, blue: 23.0/255.0, alpha: 1.0)
    }
}
